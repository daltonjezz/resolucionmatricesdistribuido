#ifndef _FUNCIONESDB_H
#define	_FUNCIONESDB_H

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sqlite3.h>

sqlite3 * DB_conectar(int flag);
void DB_desconectar(sqlite3 *handle);
int DB_Exec_Query(sqlite3 *handle, char *query);
void inicializar_db ();

#endif	/* _FUNCIONESDB_H */
