#include "funcionesDB.h"
/*
----------------------------------------------------------------------------------------
FUNCIONES PARA EL MANEJO DE LA BASE DE DATOS
----------------------------------------------------------------------------------------
*/

/*
-------------------------------------------
CONEXION A LA BASE DE DATOS
-------------------------------------------
*/
sqlite3 * DB_conectar(int flag){

  sqlite3 *handle;

  int retval = sqlite3_open_v2("BD.db3",&handle,flag,0);

  if(retval)
  {
    printf("----Error '%i' al intentar conectarse a la base de datos----\n",retval);
  }
  else
  {
    printf("----Conectado con exito a la BD----\n");
  }

  return handle; //nos devuelve el puntero para manejar las BD
}
/*
-------------------------------------------
DESCONEXION A LA BASE DE DATOS
-------------------------------------------
*/
void DB_desconectar( sqlite3 *handle ){

    sqlite3_close(handle); //Cierra la conexion a la BD.
}
/*
-------------------------------------------
EJECUTA LAS CONSULTAS DE INSERTAR, ACTUALIZAR,ELIMINAR
-------------------------------------------
*/
int DB_Exec_Query(sqlite3 *handle, char *query){

  srand((unsigned)time(0));
  int retval,i;

  retval = sqlite3_exec(handle, query, 0, 0, 0);

  while (retval == SQLITE_BUSY) {
    retval = rand()%1000;
    for(i=0; i<retval; i++);
    retval = sqlite3_exec(handle, query, 0, 0, 0);
  }

  return retval;
}
/*
-------------------------------------------
INICIALIZA LA BASE DE DATOS ELIMINANDO TODOS
-------------------------------------------
*/
void inicializar_db ()
{
    sqlite3 *handle;
    int retval;
    char query[255];

    handle = DB_conectar(SQLITE_OPEN_READWRITE);

    sqlite3_exec(handle, "BEGIN", 0, 0, 0);

    sprintf(query, "DELETE FROM operaciones;");
    retval = DB_Exec_Query(handle, query);

    sprintf(query, "DELETE FROM suboperaciones;");
    retval = DB_Exec_Query(handle, query);

    DB_desconectar(handle);
}
