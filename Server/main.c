#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <signal.h>
#include "../funciones/funciones.h"
#include "funcionesDB.h"

int main(void)
{
	int sd;
	int sd_client;
	struct sockaddr_in servidor;
	struct sockaddr_in cliente;
  pthread_t tid;
	int lon;

		//inicializo la BD
		inicializar_db();

		servidor.sin_family = AF_INET;
		servidor.sin_port = htons (4444);
		servidor.sin_addr.s_addr = INADDR_ANY;

		//Crear Socket
		sd = socket (PF_INET, SOCK_STREAM, 0);

		//Bindear Socket
		if ( bind ( sd , (struct sockaddr *) &servidor , sizeof(servidor) ) < 0 ) {
					perror("Error en Bind\n");
					exit (-1);
				}

		//Escuchar Conexiones
		printf("----Escuchando Conexiones----\n");
		if (listen(sd,20) < 0) {
				perror("Error Listend\n");
				exit(-1);
			}

		while (1) {
			lon = sizeof(cliente);

			//Acepto una conexion
			sd_client = accept ( sd, (struct sockaddr *) &cliente, &lon );
			if (sd_client < 0) {
				perror("Error al realizar el ACCEPT");
				exit(-1);
			}

			printf("\n----Cliente conectado - Direccion IP:[port] = %s:[%u]---- \n",inet_ntoa(cliente.sin_addr),ntohs(cliente.sin_port));
		} //FIN DEL WHILE 1

		listen ( sd , 10);
}
