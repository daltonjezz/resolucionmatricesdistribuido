/***********************************************************************
*	UTN - FACULTAD REGIONAL LA PLATA
*	RESOLUCION DE MATRICES DISTRIBUIDO
*	Prof: Eijo Agustin
*	Grupo N°   - Bourgeois Agustin / Coronel, Renzo, / Tolosa, David
************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include "message.h"

int read_byte (int sd, void *buffer, int tam);
int read_message(int sd, struct messageRouse *msj);
int send_messaje(int sd, struct messageRouse *msj);

