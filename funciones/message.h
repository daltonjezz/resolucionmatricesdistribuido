/***********************************************************************
*	UTN - FACULTAD REGIONAL LA PLATA
*	RESOLUCION DE MATRICES DISTRIBUIDO
*	Prof: Eijo Agustin
*	Grupo N°   - Bourgeois Agustin / Coronel, Renzo, / Tolosa, David
************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>

//Constantes


//conexion
#define SOLICITUD_CONEXION_CLIENTE 101
#define SOLICITUD_CONEXION_TRABAJADOR 101
//operacion
#define SOLICITUD_OPERACION_SUM 201
#define SOLICITUD_OPERACION_RES 202
#define SOLICITUD_OPERACION_MUL 203
#define RESPUESTA_OPE 204
//Trabajo
#define SOLICITUD_DE_TRABAJO 301
#define ASIGNACION_TRABAJO 302
#define RESPUESTA_TRABAJO 303
//ACK
#define ACK 401
//errores
#define SERVIDOR_SIN_ESPACIO_CLIENTE 501
#define OPERACION_NO_REALIZADA 502
#define SERVIDOR_SIN_TRABAJADORES 503
#define TRABAJADOR_OCUPADO 504

struct headerMessage
{
     int codigo;
     uint32_t length;
} ;

struct dataMessage
{
	char *datos;
};

struct messageRouse
{
   struct  headerMessage header;
   struct  dataMessage body;

};

#define HEADER_LENGTH sizeof(struct  headerMessage)
