/***********************************************************************
*	UTN - FACULTAD REGIONAL LA PLATA
*	RESOLUCION DE MATRICES DISTRIBUIDO
*	Prof: Eijo Agustin
*	Grupo N°   - Bourgeois Agustin / Coronel, Renzo, / Tolosa, David
************************************************************************/

#include "funciones.h"

/********************************************************************
* FUNCION PARA LA LECTURA DE LOS BYTE DEL MENSAJE
*********************************************************************/

int read_byte (int sd, void *buffer, int tam)
{
    int bytes = 1;
    int leido = 0;

    while ( ( leido < tam ) && ( bytes != 0 ) ) {
        bytes = recv(sd, buffer+leido, (tam-leido) , 0);
        if (bytes < 0) {
             perror("Error en al leer byte");
             exit(EXIT_FAILURE);
        }
        leido += bytes;
    }
  return leido;
}

/********************************************************************
* FUNCION PARA LA LECTURA DE MENSAJES
*********************************************************************/

int read_message(int sd, struct messageRouse *msj)
{
    int n;
    char * buffer=NULL;

    n = read_byte (sd, &msj->header, HEADER_LENGTH ); //leo los datos de la cabecera
     if (msj->header.length> 0){
        buffer = (char *) malloc (sizeof(char)*(msj->header.length +1));
        if (buffer == NULL){
            perror ( "No se puede asignar memoria" );
            exit(EXIT_FAILURE);
        }
        memset(buffer,0, msj->header.length+1);
        //leo el cuerpo del mensaje
        if (n != 0) {
            n = read_byte (sd, &msj->body.datos, msj->header.length);
        }
    }

    printf ("Se recibio:\n CODIGO: %c \n LONGITUD: %d \n DATOS: %s\n\n", msj->header.codigo, msj->header.length , msj->body.datos );
    fflush(stdout);

    return (1);
}

/********************************************************************
* FUNCION PARA ENVIAR MENSAJES
*********************************************************************/


int send_messaje(int sd, struct messageRouse *msj)
{
       int n;
       uint32_t  lon;
       char *buffer;

       lon = strlen (msj->body.datos);
       msj->header.length = (htons( lon )+1) + HEADER_LENGTH ; // lo pongo en longitud para la red

       buffer = (char *) malloc ((sizeof(char) * msj->header.length));

       if (buffer== NULL){
           perror ( "No se puede asignar memoria: " );
           exit(EXIT_FAILURE);
       }

       memset(buffer,0,(sizeof(char) * msj->header.length));

       memcpy ( buffer , &msj , msj->header.length);	 // Guarda al inicio del buffer el código y longitud del mensaje


       printf ("\nDatos a enviar:\n CODIGO: %c \n LONGITUD (red): %u  -- Long host: %u \n DATOS: %s\n\n", msj->header.codigo, msj->header.length , msj->body.datos);
       n = send (sd, buffer,  msj->header.length , 0);	// envia los datos!

       free(buffer);

       return n;
}
